package com.cnbbx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author jinge
 */
@EnableEurekaServer
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableWebSecurity
@Controller
public class CenterApplication {

    private static Logger logger = LoggerFactory.getLogger(CenterApplication.class);

    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    public static void main(String[] args) {
        SpringApplication.run(CenterApplication.class, args);
        logger.info("CenterApplication SpringBoot Start Success.");
    }

}