package com.cnbbx.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author jinge
 */
@SpringCloudApplication
@EnableFeignClients
public class ConsumerApplication {

    private static Logger logger = LoggerFactory.getLogger(ConsumerApplication.class);

    public static void main(String[] args) {
        new SpringApplicationBuilder(ConsumerApplication.class).run(args);
        logger.info("ConsumerApplication SpringBoot Start Success.");
    }

}