package com.cnbbx.consumer.controller;

import com.cnbbx.consumer.rest.RemoteInterfaceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jinge
 */
@Controller
@RequestMapping("company")
public class CompanyController {

    @Autowired
    private RemoteInterfaceFeignClient remoteInterface;

    @RequestMapping(value = "/teamInfo", method = RequestMethod.GET)
    @ResponseBody
    public String getTeamInfo(String teamName) {
        return remoteInterface.getTeamInfo(teamName);
    }

    @RequestMapping(value = "/companyInfo", method = RequestMethod.GET)
    @ResponseBody
    public String getCompanyInfo(String companyName) {
        return remoteInterface.getCompanyInfoByProvider(companyName);
    }

}