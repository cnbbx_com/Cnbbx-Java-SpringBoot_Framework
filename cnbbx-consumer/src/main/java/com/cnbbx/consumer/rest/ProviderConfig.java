package com.cnbbx.consumer.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jinge
 */
@Configuration
public class ProviderConfig {

    @Bean
    public RemoteInterfaceFeignClientFallbackFactory factory(){
        return new RemoteInterfaceFeignClientFallbackFactory();
    }

}
