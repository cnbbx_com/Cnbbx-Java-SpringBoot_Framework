package com.cnbbx.consumer.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author jinge
 */
@FeignClient(name = "cnbbx-provider",  fallbackFactory = RemoteInterfaceFeignClientFallbackFactory.class, configuration = ProviderConfig.class)
public interface RemoteInterfaceFeignClient {

    /**
     * 获取公司信息
     *
     * @param companyName
     * @return
     */
    @RequestMapping(value = "/getCompanyInfoByProvider", method = RequestMethod.GET)
    String getCompanyInfoByProvider(@RequestParam("companyName") String companyName);


    /**
     * 获取公司信息
     *
     * @param teamName
     * @return
     */
    @RequestMapping(value = "/getTeamInfo", method = RequestMethod.GET)
    String getTeamInfo(@RequestParam("teamName") String teamName);

}