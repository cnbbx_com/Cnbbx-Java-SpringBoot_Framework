package com.cnbbx.consumer.rest;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author jinge
 */
@Component
public class RemoteInterfaceFeignClientFallbackFactory implements FallbackFactory<RemoteInterfaceFeignClient> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public RemoteInterfaceFeignClient create(Throwable cause) {
        return new RemoteInterfaceFeignClient() {

            @Override
            public String getCompanyInfoByProvider(String companyName) {
                logger.info("fallback; reason was:" + cause);
                return "服务器挂了";
            }

            @Override
            public String getTeamInfo(String teamName) {
                logger.info("fallback; reason was:" + cause);
                return "服务器挂了";
            }

        };
    }

}
