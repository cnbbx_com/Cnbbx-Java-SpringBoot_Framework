package com.cnbbx.provider.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jinge
 */
@Controller
public class CompanyProviderController {

    @RequestMapping(value = "/getCompanyInfoByProvider", method = RequestMethod.GET)
    @ResponseBody
    public String getCompanyInfoByProvider(@RequestParam("companyName") String companyName) {
        return "provider 查询" + companyName + "公司信息";
//        throw new RuntimeException("hello world");
    }

    @RequestMapping(value = "/getTeamInfo", method = RequestMethod.GET)
    @ResponseBody
    public String getTeamInfo(@RequestParam("teamName") String teamName) {
        return "provider 查询" + teamName + "团队信息";
    }

}